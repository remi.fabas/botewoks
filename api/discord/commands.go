package discord

var (
	integerOptionMinValue = 1.0
	/*commands              = []*discordgo.ApplicationCommand{
		{
			Name:        "crancor",
			Description: "Command for crancor damage",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "phase",
					Description: "1,2,3 or 4",
					Required:    true,
					MinValue:    &integerOptionMinValue,
					MaxValue:    4,
				},
				{
					Type:        discordgo.ApplicationCommandOptionNumber,
					Name:        "value",
					Description: "value: ex 1542124 or 12.5",
					MinValue:    &integerOptionMinValue,
					MaxValue:    999999999999999,
					Required:    true,
				},
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "info",
					Description: "ex: team padme",
					Required:    false,
				},
			},
		},
		{
			Name:        "crancor2",
			Description: "only for testing",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "phase",
					Description: "1,2,3 or 4",
					Required:    true,
					MinValue:    &integerOptionMinValue,
					MaxValue:    4,
				},
				{
					Name:        "type",
					Description: "raw damage or percentage.",
					Required:    true,
					Type:        discordgo.ApplicationCommandOptionInteger,
					Choices: []*discordgo.ApplicationCommandOptionChoice{
						{
							Name:  "Raw damage",
							Value: 1,
						},
						{
							Name:  "Percentage",
							Value: 2,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionNumber,
					Name:        "value",
					Description: "value: ex 1542124 or 12.5",
					MinValue:    &integerOptionMinValue,
					MaxValue:    999999999999999,
					Required:    true,
				},
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "info",
					Description: "ex: team padme",
					Required:    false,
				},
			},
		},
		{
			Name:        "speedmod",
			Description: "only for testing",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "allycode",
					Description: "ex: 327786519",
					Required:    true,
					MinValue:    &integerOptionMinValue,
					MaxValue:    999999999,
				},
			},
		},
	}*/
)
