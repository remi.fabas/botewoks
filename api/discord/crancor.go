package discord

import (
	"fmt"
	"log"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

type CrancorHandler struct{}

var (
	CrancorCmdName = "crancor"
	CrancorCmd     = discordgo.ApplicationCommand{
		Name:        CrancorCmdName,
		Description: "Command for crancor damage",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionInteger,
				Name:        "phase",
				Description: "1,2,3 or 4",
				Required:    true,
				MinValue:    &integerOptionMinValue,
				MaxValue:    4,
			},
			{
				Type:        discordgo.ApplicationCommandOptionNumber,
				Name:        "value",
				Description: "value: ex 1542124 or 12.5",
				MinValue:    &integerOptionMinValue,
				MaxValue:    999999999999999,
				Required:    true,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "info",
				Description: "ex: team padme",
				Required:    false,
			},
		},
	}
)

func (c *CrancorHandler) Crancor(s *discordgo.Session, i *discordgo.InteractionCreate) {
	logMsg := "crancor command for"
	log.Print(logMsg)
	// Access options in the order provided by the user.
	options := i.ApplicationCommandData().Options

	// Or convert the slice into a map
	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	// This example stores the provided arguments in an []interface{}
	// which will be used to format the bot's response
	margs := make([]interface{}, 0, len(options))
	msgformat := "```\n"

	// Get the value from the option map.
	// When the option exists, ok = true
	if option, ok := optionMap["phase"]; ok {
		// Option values must be type asserted from interface{}.
		// Discordgo provides utility functions to make this simple.
		phase := option.IntValue()
		margs = append(margs, phase)
		msgformat += "> phase: %d\n"

		if opt, ok := optionMap["value"]; ok {
			margs = append(margs, strconv.FormatFloat(opt.FloatValue(), 'f', 0, 64))
			msgformat += "> damage: %s\n"
			percentage := calculatePercentage(phase, opt.FloatValue())
			margs = append(margs, strconv.FormatFloat(percentage, 'f', 2, 64))
			msgformat += "> percentage: %s\n"
		}
		if opt, ok := optionMap["info"]; ok {
			margs = append(margs, opt.StringValue())
			msgformat += "> info: %s\n"
		}
	}
	msgformat += "```\n"
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		// Ignore type for now, they will be discussed in "responses"
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: fmt.Sprintf(
				msgformat,
				margs...,
			),
		},
	})
}

func calculatePercentage(phase int64, damage float64) float64 {
	switch phase {
	case 1:
		return damage / 41193998 * 100
	case 2:
		return damage / 36425856 * 100
	case 3:
		return damage / 39461352 * 100
	case 4:
		return damage / 37943604 * 100
	}
	return 0
}

func calculateDamage(phase int64, percent float64) float64 {
	switch phase {
	case 1:
		return percent / 100 * 41193998
	case 2:
		return percent / 100 * 36425856
	case 3:
		return percent / 100 * 39461352
	case 4:
		return percent / 100 * 37943604
	}
	return 0
}
