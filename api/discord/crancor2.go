package discord

import (
	"fmt"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

type crancor2Handler struct{}

var (
	Crancor2CmdName = "crancor2"
	Crancor2Cmd     = discordgo.ApplicationCommand{
		Name:        "crancor2",
		Description: "only for testing",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionInteger,
				Name:        "phase",
				Description: "1,2,3 or 4",
				Required:    true,
				MinValue:    &integerOptionMinValue,
				MaxValue:    4,
			},
			{
				Name:        "type",
				Description: "raw damage or percentage.",
				Required:    true,
				Type:        discordgo.ApplicationCommandOptionInteger,
				Choices: []*discordgo.ApplicationCommandOptionChoice{
					{
						Name:  "Raw damage",
						Value: 1,
					},
					{
						Name:  "Percentage",
						Value: 2,
					},
				},
			},
			{
				Type:        discordgo.ApplicationCommandOptionNumber,
				Name:        "value",
				Description: "value: ex 1542124 or 12.5",
				MinValue:    &integerOptionMinValue,
				MaxValue:    999999999999999,
				Required:    true,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "info",
				Description: "ex: team padme",
				Required:    false,
			},
		},
	}
)

func (c *crancor2Handler) Crancor2(s *discordgo.Session, i *discordgo.InteractionCreate) {
	options := i.ApplicationCommandData().Options
	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	margs := make([]interface{}, 0, len(options))
	msgformat := "```\n"

	if option, ok := optionMap["phase"]; ok {
		phase := option.IntValue()
		margs = append(margs, phase)
		msgformat += "> phase: %d\n"

		if opt, ok := optionMap["info"]; ok {
			margs = append(margs, opt.StringValue())
			msgformat += "> info: %s\n"
		}

		switch i.ApplicationCommandData().Options[1].IntValue() {
		case int64(1):
			if opt, ok := optionMap["value"]; ok {
				margs = append(margs, strconv.FormatFloat(opt.FloatValue(), 'f', 0, 64))
				msgformat += "> damage: %s\n"
				percentage := calculatePercentage(phase, opt.FloatValue())
				margs = append(margs, strconv.FormatFloat(percentage, 'f', 2, 64))
				msgformat += "> percentage: %s\n"
			}
		case int64(2):
			if opt, ok := optionMap["value"]; ok {
				margs = append(margs, opt.FloatValue())
				msgformat += "> damage: %f\n"
				dmg := calculateDamage(phase, opt.FloatValue())
				margs = append(margs, fmt.Sprintf("%f", dmg))
				msgformat += "> raw damage: %s\n"
			}
		}

	}
	msgformat += "```\n"
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		// Ignore type for now, they will be discussed in "responses"
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: fmt.Sprintf(
				msgformat,
				margs...,
			),
		},
	})

}
