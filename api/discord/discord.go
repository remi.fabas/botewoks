package discord

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

type DiscordHandler struct {
	GuildID         string
	BotToken        string
	Session         *discordgo.Session
	Commands        []*discordgo.ApplicationCommand
	CommandHandlers map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)
}

func NewDiscordHandler(guildID string, botToken string) (DiscordHandler, error) {
	s, err := discordgo.New("Bot " + botToken)
	if err != nil {
		return DiscordHandler{}, err
	}

	commandHandlers := make(map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate))
	return DiscordHandler{
		GuildID:  guildID,
		BotToken: botToken,
		Session:  s,
		//Commands:        commands,        // From api/discord/commands.go
		CommandHandlers: commandHandlers, // From api/discord/commands_handler.go
	}, nil
}

func (d *DiscordHandler) SyncDiscord() {

	// Declare custom commands
	c := CrancorHandler{} // right now not usefull
	d.Commands = append([]*discordgo.ApplicationCommand{}, &CrancorCmd)
	d.CommandHandlers[CrancorCmdName] = c.Crancor

	c2 := crancor2Handler{}
	d.Commands = append(d.Commands, &Crancor2Cmd)
	d.CommandHandlers[Crancor2CmdName] = c2.Crancor2

	// will upload to discord our custom cmd
	d.Session.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if h, ok := d.CommandHandlers[i.ApplicationCommandData().Name]; ok {
			h(s, i)
		}
	})
	d.Session.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) { // log when bot start
		log.Printf("Logged in as: %v#%v", s.State.User.Username, s.State.User.Discriminator)
	})
}

func (d *DiscordHandler) Open() error {
	err := d.Session.Open()
	if err != nil {
		return err
	}

	log.Printf("Adding %v commands...\n", len(d.Commands))
	for _, v := range d.Commands {
		_, err := d.Session.ApplicationCommandCreate(d.Session.State.User.ID, "", v)
		if err != nil {
			log.Panicf("Cannot create '%v' command: %v", v.Name, err)
		}
	}
	return nil
}
