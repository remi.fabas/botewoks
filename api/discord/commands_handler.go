package discord

/*var (
	commandHandlers = map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
		"crancor": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			log.Print("crancor command handled.")
			// Access options in the order provided by the user.
			options := i.ApplicationCommandData().Options

			// Or convert the slice into a map
			optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
			for _, opt := range options {
				optionMap[opt.Name] = opt
			}

			// This example stores the provided arguments in an []interface{}
			// which will be used to format the bot's response
			margs := make([]interface{}, 0, len(options))
			msgformat := "```\n"

			// Get the value from the option map.
			// When the option exists, ok = true
			if option, ok := optionMap["phase"]; ok {
				// Option values must be type asserted from interface{}.
				// Discordgo provides utility functions to make this simple.
				phase := option.IntValue()
				margs = append(margs, phase)
				msgformat += "> phase: %d\n"

				if opt, ok := optionMap["value"]; ok {
					margs = append(margs, strconv.FormatFloat(opt.FloatValue(), 'f', 0, 64))
					msgformat += "> damage: %s\n"
					percentage := calculatePercentage(phase, opt.FloatValue())
					margs = append(margs, strconv.FormatFloat(percentage, 'f', 2, 64))
					msgformat += "> percentage: %s\n"
				}
				if opt, ok := optionMap["info"]; ok {
					margs = append(margs, opt.StringValue())
					msgformat += "> info: %s\n"
				}
			}
			msgformat += "```\n"
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				// Ignore type for now, they will be discussed in "responses"
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: fmt.Sprintf(
						msgformat,
						margs...,
					),
				},
			})
		},
		"crancor2": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			options := i.ApplicationCommandData().Options
			optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
			for _, opt := range options {
				optionMap[opt.Name] = opt
			}

			margs := make([]interface{}, 0, len(options))
			msgformat := "Yaa-yaah ! Coatee-cha tu yub nub !\n"

			if option, ok := optionMap["phase"]; ok {
				phase := option.IntValue()
				margs = append(margs, phase)
				msgformat += "> phase: %d\n"

				if opt, ok := optionMap["info"]; ok {
					margs = append(margs, opt.StringValue())
					msgformat += "> info: %s\n"
				}

				switch i.ApplicationCommandData().Options[1].IntValue() {
				case int64(1):
					if opt, ok := optionMap["value"]; ok {
						margs = append(margs, opt.FloatValue())
						msgformat += "> damage: %f\n"
						percentage := calculatePercentage(phase, opt.FloatValue())
						margs = append(margs, fmt.Sprintf("%f", percentage))
						msgformat += "> percentage: %s\n"
					}
				case int64(2):
					if opt, ok := optionMap["value"]; ok {
						margs = append(margs, opt.FloatValue())
						msgformat += "> damage: %f\n"
						dmg := calculateDamage(phase, opt.FloatValue())
						margs = append(margs, fmt.Sprintf("%f", dmg))
						msgformat += "> raw damage: %s\n"
					}
				}

			}

			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				// Ignore type for now, they will be discussed in "responses"
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: fmt.Sprintf(
						msgformat,
						margs...,
					),
				},
			})

		},
		"speedmod": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			log.Print("speedmod command handled")
			options := i.ApplicationCommandData().Options
			optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
			for _, opt := range options {
				optionMap[opt.Name] = opt
			}

			margs := make([]interface{}, 0, len(options))
			msgformat := "```\n"

			if allycode, ok := optionMap["allycode"]; ok {
				log.Print("contacting wswgoh")
				resp, err := http.Get("http://wswgoh.default.svc.cluster.local:8080/speedmods/" + fmt.Sprint(allycode.IntValue()))
				if err != nil {
					fmt.Println(err)
					return
				}
				defer resp.Body.Close()

				body, err := ioutil.ReadAll(resp.Body)

				if err != nil {
					fmt.Println(err)
					return
				}

				fmt.Println(string(body))
				margs = append(margs, string(body))
				msgformat += "> speedmods: %s\n"
			}

			msgformat += "```\n"
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				// Ignore type for now, they will be discussed in "responses"
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: fmt.Sprintf(
						msgformat,
						margs...,
					),
				},
			})

		},
	}
)
*/
