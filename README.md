# Bot for Ewoks !

Small bot for SWGOH Game, Ewoks guild. This is for testing, not production ready.

## Discord Developer Portal
https://discord.com/developers/docs/intro
needed to create your application/bot and retrieve the DISCORD_TOKEN used later

## Build docker image
`docker build . -t botewoks` or  
`docker build . -t remifabas/botewoks:v1.0.0` usefull later

## Launch with docker
`docker run --env DISCORD_TOKEN=$DISCORD_TOKEN --detach --name botewoks-container botewoks`

## Deploy Infra
You can deploy a cluster locally with kind
Use kind to create a cluster k8s with docker containers: https://kind.sigs.k8s.io/
Then apply the cluster configuration: `kind create cluster --name local-cluster --config infra/kind-cluster.yaml`


### Add your discord token to the deployment
`mkdir -p tmp/k8s && sed "s/SECRET_DISCORD_TOKEN/${DISCORD_TOKEN}/g" k8s/deployment.yaml > tmp/k8s/deployment.yaml`
## Deploy k8s resources
first load images on your cluster (i.e node will know these docker images)
`kind load docker-image remifabas/botewoks:v1.0.0 --name local-cluster`

You can see a list of images loaded on a specific node with:
`docker exec -it my-node-name crictl images`

Then apply your k8s manifest 
`k apply -f tmp/k8s/deployment.yaml`

About latest tag and imagePullPolicy:
-> do not use latest and/or imagePullPolicy: Always with local registry kubelet will search in docker hub or web registry and will fail
The Kubernetes default pull policy is IfNotPresent unless the image tag is :latest or omitted (and implicitly :latest) in which case the default policy is Always. IfNotPresent causes the Kubelet to skip pulling an image if it already exists. If you want those images loaded into node to work as expected, please:
don't use a :latest tag
and / or:
specify imagePullPolicy: IfNotPresent or imagePullPolicy: Never on your container(s).
See Kubernetes imagePullPolicy for more information.


## monitoring stack
Create the namespace and CRDs, and then wait for them to be available before creating the remaining resources
`kubectl apply --server-side -f k8s/monitoring/manifests/setup`
`kubectl apply -f k8s/monitoring/manifests/`

### View grafana dashboard
You can access the dashboards by using port-forward to access Grafana. It does not have a public endpoint for security reasons
`kubectl -n monitoring port-forward svc/grafana 3000`

### View prometheus
`kubectl -n monitoring port-forward svc/prometheus-operated 9090`

### Check Service Monitors
To see how Prometheus is configured on what to scrape , we list service monitors
`kubectl -n monitoring get servicemonitors`

Describe specific servicemonitor
`kubectl -n monitoring describe servicemonitor node-exporter`
