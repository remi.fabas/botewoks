FROM golang:1.18.2 AS builder

COPY go.mod /go/src/remifabas/botewoks/go.mod
ADD vendor /go/src/remifabas/botewoks/vendor
ADD api /go/src/remifabas/botewoks/api
ADD main.go /go/src/remifabas/botewoks/main.go

WORKDIR /go/src/remifabas/botewoks/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /go/bin/botewoks

FROM alpine:latest
COPY --from=builder /go/bin/botewoks /go/bin/botewoks
ENTRYPOINT ["/go/bin/botewoks"]
