package main

import (
	"log"
	"os"
	"os/signal"

	d "gitlab.com/remifabas/botewoks/api/discord"
)

var (
	dHandler d.DiscordHandler
)

func init() {
	var err error
	dHandler, err = d.NewDiscordHandler("", os.Getenv("DISCORD_TOKEN"))
	if err != nil {
		log.Fatalf("Invalid bot parameters: %v", err)
	}
	dHandler.SyncDiscord()
}

func gracefullyShutting() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	log.Println("Press Ctrl+C to exit")
	<-stop
	log.Println("Gracefully shutting down.")
}

func main() {
	err := dHandler.Open()
	if err != nil {
		log.Fatalf("Cannot open the session: %v", err)
	}
	defer dHandler.Session.Close()
	gracefullyShutting()
}
